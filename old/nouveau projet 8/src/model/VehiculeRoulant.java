package model;
public abstract class VehiculeRoulant implements YIVehiculeRoulant {   
	//attributs    
	
	protected int longueur,largeur;     
	protected String nom;           

	//getter / setters    
	
	public int getLongueur() {     
		return longueur;         }    
	public void setLongueur(int longueur) {       
		this.longueur = longueur;         }        
	public int getLargeur() {        
		return largeur;         }       
	public void setLargeur(int largeur) { 
		this.largeur = largeur;         }    
	public String getNom() {           
		return nom;         }       
	public void setNom(String nom) {      
		this.nom = nom;         }       
	
	//methode toString 
	
	
	@Override      
	public String toString() {       
		return "Voitures [nom=" + nom + "]";         }   
	
	//methodes abstraites 
	        
	
}
