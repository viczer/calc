package model;

public class Voitures extends VehiculeRoulant {

	
		public Voitures(int lon, int lar, String nom) {
			super();
			this.longueur = lon;
			this.largeur = lar;
			this.nom = nom;
		}
		
		
		@Override
		public String toString() {
			return "Voitures [lon=" + longueur + ", lar=" + largeur + ", nom=" + nom + "]";
		}


		public Voitures() {
			super();
			// TODO Auto-generated constructor stub
		}

		
		public void appuyerAccelerateur(int distance) {
			System.out.println(this + " ");
			for (int i=0; i<distance; ++i)
				System.out.print("-");
				System.out.println();
		}
		
		
		
		
	public void tournerVolant(int degres) {
		System.out.println(this + " tourne de " + degres + " degres ");
			}


	@Override
	public void avancer(int distance) {
		// TODO Auto-generated method stub
		appuyerAccelerateur(distance);
	}


	@Override
	public void tourner(int degres) {
		// TODO Auto-generated method stub
		tournerVolant(degres);
		
	}


	@Override
	public char getLogo() {
		// TODO Auto-generated method stub
		return 'X';
	}
		
}
