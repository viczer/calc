package model;

public class Park {

	private Parking [] places;

	public Park(int nb) {
		// on cree le tableau de (reference vers) places de parking
		places = new Parking [nb];

		// on cree les places 

		places[0] = new Parking(1, 1, null);
		places[1] = new Parking(1, 1, null);
		for (int i=2; i<places.length; ++i) {
			places[i] = new Parking(2, 1, null);
		}
	}

	




	@Override
	public String toString() {
		String ret = "Parking";
		for (int i=0; i<places.length; ++i)
			ret += " | "+places[i].toString();
		ret += " | ";
		return ret;

	}






	public boolean entrer(YIVehiculeRoulant v) {
		int pos = -1;
		for (int i=0; i<places.length; ++i) {
			if (places[i].estLibre() &&
					places[i].peutTenir(v)) {
				pos = i;
				break;
			}
		}

		// si pas de places trouve ==> on renvoie a false

		if  (pos == -1)
			return false;
		// ici on a trouver une place
		// on deplace la voiture jusqua la place

		v.avancer(pos);
		v.tourner(90);
		v.avancer(1);
		//enregistre la voiture dans la place
		places[pos].setVehicule(v);
		// renvoie true 
		return true;
	}
	public boolean sortir(VoitureAmphibie va) {
		for (int i=0; i<places.length; ++i) {
			if (places[i].getVehicule() == va) {
			
				
				va.avancer(-1);
				va.tourner(90);
				va.avancer(i);

				places[i].setVehicule(null);
				return true;
			}


		}
		return false;
	}






	public void sortir(Moto m1) {
		// TODO Auto-generated method stub
		
	}






	public void sortir(Voitures v1) {
		// TODO Auto-generated method stub
		
	}






	public void sortir(Bateau b1) {
		// TODO Auto-generated method stub
		
	}
}