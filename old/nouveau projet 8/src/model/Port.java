package model;

public class Port {

	private PlaceDePort[] places;

	public Port(int nb) {
		// on cree le tableau de (reference vers) places de parking
		places = new PlaceDePort [nb];

		// on cree les places 
		for (int i=0; i<2; ++i)
		places[i] = new PlaceDePort(5, 2, 2, null);
		
		for (int i=2; i<places.length; ++i) {
			places[i] = new PlaceDePort(10, 5, 1, null);
		}
	}

	




	@Override
	public String toString() {
		String ret = "Port";
		for (int i=0; i<places.length; ++i)
			ret += " | "+places[i].toString();
		ret += " | ";
		return ret;

	}






	public boolean entrer(Bateau b1) {
		int pos = -1;
		for (int i=0; i<places.length; ++i) {
			if (places[i].estLibre() &&
					places[i].peutTenir((VehiculeNav) b1)) {
				pos = i;
				break;
			}
		}

		// si pas de places trouve ==> on renvoie a false

		if  (pos == -1)
			return false;
		// ici on a trouver une place
		// on deplace la voiture jusqua la place
		b1.naviguer(1);
		b1.virerDeBord(90);
		b1.naviguer(pos);

		//enregistre la voiture dans la place
		places[pos].setVehicule(b1);
		// renvoie true 
		return true;
	}
	public boolean sortir(VehiculeNav v) {
		for (int i=0; i<places.length; ++i) {
			if (places[i].getVehicule() == v) {
			
				
				v.naviguer(-1);
				v.virerDeBord(90);
				v.naviguer(i);

				places[i].setVehicule(null);
				return true;
			}


		}
		return false;
	}






	public void entrer(VoitureAmphibie va) {
		// TODO Auto-generated method stub
		
	}
}