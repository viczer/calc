package model;

public class Parking {

	 private int lonMax;
	 private int larMax;
	 private YIVehiculeRoulant vehicule;

	 public Parking(int lonMax, int larMax, YIVehiculeRoulant vehicule) {
		super();
		this.lonMax = lonMax;
		this.larMax = larMax;
		this.vehicule = vehicule;
		
	}
	public Parking() {
		super();
		// TODO Auto-generated constructor stub
	}
	public YIVehiculeRoulant getVehicule() {
		return vehicule;
	}
	public void setVehicule(YIVehiculeRoulant vehicule) {
		this.vehicule = vehicule;
	}
	 
	public String toString() {
		if (estLibre())
			return" ";
		else
        return ""+vehicule.getLogo();	 
	}
		public boolean estLibre() {
			return (vehicule == null);
		}
	 
		public boolean peutTenir(YIVehiculeRoulant v) {
			if ((lonMax >= v.getLongueur()) &&
				(larMax >= v.getLargeur()))
				return true;
			else
				return false;
			
		}
	 
}

