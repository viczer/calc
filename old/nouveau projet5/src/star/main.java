package star;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		int solution, essai, nbEssais=0;
		final int MAX = 10;
		int [] essaisPrecedents = new int[MAX];
		solution= (int) (Math.random()*99+1);

		Scanner s= new Scanner(System.in);
		System.out.println("essayer de deviner le nombre entre 1 et 100 :");
		do {	
			essai = s.nextInt();
			essaisPrecedents[nbEssais]= essai;
			nbEssais++;
			if (essai < solution)
				System.out.println("rat�, trop petit ! t es nul (" + nbEssais + " essais, " + (MAX - nbEssais) + " restant)");
			else if (essai > solution)
				System.out.println("rat�, trop grand ! t es nul (" + nbEssais + " essais, " + (MAX - nbEssais) + " restant)");
			if (essai != solution)
				System.out.println("rat�, trop " + ((essai < solution) ? "petit" : "grand") + " essaie");

		} while ((essai != solution) && (nbEssais< MAX));
		if (essai== solution)
			System.out.println("bravo ! (en " + nbEssais + " essais)");
		else System.out.println("perdu ! (en " + nbEssais + " essais)");
		
		for (int  x : essaisPrecedents)
			System.out.println();
			
		s.close();
	}	
}	








