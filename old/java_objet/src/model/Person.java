package model;

public class Person {
	private int num;
	private String nom;
	private String prenom;
	private Adresse adresse;

	public Adresse getAdresse() {
		return adresse;
	}
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	static private int nbPersonnes = 0;





	@Override
	public String toString() {
		return "Person [num=" + num + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse +"]";
	}
	public void setNum(int num) {
		if (num <0) this.num=0;
		else this.num = num;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Person(int num, String nom, String prenom, Adresse adresse) {
		super();
		if (num <0) this.num=0;
		else this.num = num;
		this.num = num;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		nbPersonnes++;
		
	}
	public int getNum() {
		return num;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Person() {
		super();
		nbPersonnes++;
	}
	public static int getNbPersonnes() {
		return nbPersonnes;
	}












}
