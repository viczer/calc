package maina;

import java.util.Scanner;

public class Main {
		
	public static void main(String[] args)  {
		
		int [] tab = new int[5];
		
		Scanner s= new Scanner(System.in);
		System.out.println("saisir les valeurs du tableau:");
		for (int i=0; i<tab.length; ++i) {
			tab[i] = s.nextInt();
		}
			
		System.out.print("tab = { ");		
		for (int x : tab) {
		System.out.print(x+" ");
		}
		System.out.print("}\n");
		
		int somme=0, produit = 1;
		for (int x : tab ) {
			somme +=x;
			produit*= x;
		}
		System.out.println("somme = " + somme + "produit=" + produit);
		
		
			s.close();
		}
		
	}

