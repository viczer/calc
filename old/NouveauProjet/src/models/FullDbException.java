package models;

public class FullDbException extends Exception {
	
	public FullDbException(String message) {
		super(message);
}
}
