package models;

public class Borne {

	protected Barriere barriere = new Barriere();
	
	
	protected Capteur capteur = new Capteur();

	protected ITicketDB db;
	public Barriere getBarriere() {
		return barriere;
	}


	public void setBarriere(Barriere barriere) {
		this.barriere = barriere;
	}


	public Capteur getCapteur() {
		return capteur;
	}


	public void setCapteur(Capteur capteur) {
		this.capteur = capteur;
	}
	public void setDb(ITicketDB db) {
		this.db = db;
	}
}
