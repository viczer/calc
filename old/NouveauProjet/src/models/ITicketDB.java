package models;

public interface ITicketDB {



	
	public void save(Ticket t) throws FullDbException;
		
		

	public Ticket find(int id) throws IdNotFoundException;
				
	
	public void delete(int id) throws IdNotFoundException;
}