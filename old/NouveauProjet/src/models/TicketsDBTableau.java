package models;

public class TicketsDBTableau implements ITicketDB {
	private Ticket [] tickets = new Ticket[5];
	
	private int nbTicket;
	
	
	 @Override
	public void save(Ticket t) throws FullDbException {
		if (tickets.length == nbTicket)
		throw new FullDbException("la db est pleine");
		tickets[nbTicket] = t;	
		nbTicket++;
		
	
	} 
	 @Override
	 
	public Ticket find(int id) throws IdNotFoundException {
		for (int i= 0; i<tickets.length; ++i) {
			if (id == tickets[i].getId()) {
				return tickets[i];
				
			}
		}


		throw new IdNotFoundException(id);
		
}
	 @Override
	public void delete(int id) throws IdNotFoundException {
		for (int i= 0; i<nbTicket; ++i) {
			if (id == tickets[i].getId()) {
				tickets[i] = tickets[nbTicket -1]; 
				tickets[nbTicket-1] = null;
				nbTicket--;
				return;
				}
			}
		
		throw new IdNotFoundException(id, "Id" + id + " not found while deleting from DB");
}
	public Ticket [] getTickets() {
	return tickets;
}
	public void setTickets(Ticket [] tickets) {
	this.tickets = tickets;
	
	 
}
	public String toString() {
		String ret = "";
		for (int i=0; i<tickets.length; ++i)
		ret += "\t"+tickets[i]+"\n";
		return ret;
	}
	
	
}
