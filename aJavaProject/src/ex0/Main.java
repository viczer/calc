/**�crivez un programme Java qui demande son nom � l�utilisateur et qui affiche �Bonjour� �
l'�cran et le nom sur une ligne distincte.*/
package ex0;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {

			System.out.print("Enter your name: ");
			String name = scanner.nextLine();
			System.out.printf("Welcome " + name);

		}
	}
}
