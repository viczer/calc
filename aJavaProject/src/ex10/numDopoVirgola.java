/**�crivez un programme Java qui lit un nombre � virgule et affiche "z�ro" si le nombre est z�ro. 
Sinon, affiche "positif" ou "n�gatif". 
Puis, affiche "petit" si la valeur absolue du nombre est inf�rieur � 1 ou "grand" s'il d�passe 1 000 000*/

package ex10;
import java.util.Scanner;

public class numDopoVirgola {

	public static void main(String[] args) {
		
	        Scanner n = new Scanner(System.in);
	        System.out.print("Input value: ");
	        double input = n.nextDouble();

	        if (input > 0)
	        {
	            if (input < 1)
	            {
	                System.out.println("Positive small number");
	            }
	            else if (input > 1000000)
	            {
	                System.out.println("Positive large number");
	            }
	            else
	            {
	                System.out.println("Positive number");
	            }
	        }
	        else if (input < 0)
	        {
	            if (Math.abs(input) < 1)
	            {
	                System.out.println("Negative small number");
	            }
	            else if (Math.abs(input) > 1000000)
	            {
	                System.out.println("Negative large number");
	            }
	            else
	            {
	                System.out.println("Negative number");
	            }
	        }
	        else
	        {
	            System.out.println("Zero");
	        }
	       n.close();
	    }

	}


