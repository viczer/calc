//Demandez trois nombres de l'utilisateur et affichez le plus grand nombre. 
package ex10;

import java.util.Scanner;

public class Max {

	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {

			System.out.print("Scegliere primo numero : ");
			int a = scanner.nextInt();
			
			System.out.print("Scegliere secondo numero : ");
			int b = scanner.nextInt();
			
			System.out.print("Scegliere terzo numero : ");
			int c = scanner.nextInt();

			if (a > b && a > c) {			
				System.out.println("MAX "+ a);
			}
			else if (b > a && b > c) {
				System.out.println("MAX "+ b);
			}
			else {
				System.out.println("MAX "+ c);
			}
		
	}

}
}
