package ex10;

import java.util.*;

public class Month {

	public static void main(String[] args) {
		int numberDays = 0;
		String MonthName = "Unknown";

		Scanner sc = new Scanner(System.in);
		System.out.print("Input a year: ");
		int year = sc.nextInt();
		System.out.print("Input a month number: ");
		int month = sc.nextInt();

		sc.close();

		switch (month) {
		case 1:
			MonthName = "January";
			numberDays = 31;
			break;
		case 2:
			MonthName = "February";
			if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
				numberDays = 29;
			} else {
				numberDays = 28;
			}
			break;
		case 3:
			MonthName = "March";
			numberDays = 31;
			break;
		case 4:
			MonthName = "April";
			numberDays = 30;
			break;
		case 5:
			MonthName = "May";
			numberDays = 31;
			break;
		case 6:
			MonthName = "June";
			numberDays = 30;
			break;
		case 7:
			MonthName = "July";
			numberDays = 31;
			break;
		case 8:
			MonthName = "August";
			numberDays = 31;
			break;
		case 9:
			MonthName = "September";
			numberDays = 30;
			break;
		case 10:
			MonthName = "October";
			numberDays = 31;
			break;
		case 11:
			MonthName = "November";
			numberDays = 30;
			break;
		case 12:
			MonthName = "December";
			numberDays = 31;
		}
		 System.out.print(MonthName + " " + year + " has " + numberDays + " days\n");
	}
}
