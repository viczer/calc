package main;
import java.io.FileInputStream;
import java.util.Scanner;

public class CsvReader {

    public static final int COLUMN_COUNT = 4;

    public static void main( String[] args ) throws Exception {

        try ( FileInputStream inputStream = new FileInputStream( "csv/data.csv" ) ;
              Scanner scanner = new Scanner( inputStream ) ) {
            
            scanner.useDelimiter( "[,;\\n]" );
            
            String identifier = scanner.next();
            String firstName = scanner.next();
            String lastName = scanner.next();
            String email = scanner.next();
            
            String segment = "--------------------";
            System.out.printf( "+%s+%s+%s+%s+\n",
                    segment, segment, segment, segment );
            System.out.printf( "|%-20s|%-20s|%-20s|%-20s|\n",
                    identifier, firstName, lastName, email );
            System.out.printf( "+%s+%s+%s+%s+\n",
                    segment, segment, segment, segment );

            while( scanner.hasNext() ) {
                identifier = scanner.next();
                firstName = scanner.next();
                lastName = scanner.next();
                email = scanner.next();
                
                System.out.printf( "|%-20s|%-20s|%-20s|%-20s|\n",
                        identifier, firstName, lastName, email );
            }
            
            System.out.printf( "+%s+%s+%s+%s+\n",
                    segment, segment, segment, segment );

        }
        
    }
}


